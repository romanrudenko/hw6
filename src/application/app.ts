import express from 'express';
import cors from 'cors';
import studentRouter from './students/students.router';
import exceptionFilter from './middlewares/exceptions.filter';
import bodyParser from 'body-parser';
import path from 'path';
import groupsRouter from './groups/groups.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());

const staticFilesPath = path.join(__dirname, '../', 'public')

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentRouter);
app.use('/api/v1/groups', groupsRouter);

app.use(exceptionFilter);

export default app;
