import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IGroup } from './groups.interface';

export interface IGroupCreateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IGroup, 'id'>;
}
