import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IGroup } from './groups.interface';

export interface IGroupUpdateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IGroup>;
}
