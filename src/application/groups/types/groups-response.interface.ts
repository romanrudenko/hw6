import { IStudent } from '../../students/types/students.interface';

export interface IGroupResponseInterface {
  id: string;
  name: string;
  students: IStudent[];
}
