import ObjectID from 'bson-objectid';
import { students } from '../students/students.model';
import { IGroupResponseInterface } from './types/groups-response.interface';
import { IGroup } from './types/groups.interface';
import HttpException from '../exceptions/http-exceptions';
import { HttpStatuses } from '../enums/http-status.enums';

export const groups: IGroup[] = [
  {
    id: '648c445c6ddab95788b877d7',
    name: 'Group 1',
  },
  {
    id: '648c443a6ddab95788b877c1',
    name: 'Group 2',
  },
];

export const getAllGroups = (): IGroupResponseInterface[] => {
  const groupsWithStudents = groups.map((group) => {
    const groupStudents = students.filter(
      (student) => student.groupId === group.id,
    );
    return {
      ...group,
      students: groupStudents,
    };
  });

  return groupsWithStudents;
};

export const getGroupById = (
  groupId: string,
): IGroupResponseInterface | undefined => {
  const group = groups.find(({ id }) => groupId === id);

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Group not found");
  }

  const groupStudents = students.filter(
    (student) => student.groupId === groupId,
  );

  return { ...group, students: groupStudents };
};

export const createGroup = (createGroupSchema: Omit<IGroup, 'id'>) => {
  const newGroup = {
    id: ObjectID().toHexString(),
    ...createGroupSchema,
  };

  groups.push(newGroup);

  return newGroup;
};

export const updateGroupById = (
  groupId: string,
  updateGroupSchema: Partial<IGroup>,
): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Group not found");
  }

  const updatedGroup = {
    ...group,
    ...updateGroupSchema,
  };

  groups.splice(groupIndex, 1, updatedGroup);

  return updatedGroup;
};

export const deleteGroupById = (groupId: string): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Group not found");
  }

  groups.splice(groupIndex, 1);

  return group;
};
