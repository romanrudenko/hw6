import { Request, Response } from 'express';
import * as groupServices from './groups.services';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateInterface } from './types/group-create-request.interface';
import { IGroupUpdateInterface } from './types/group-update-request.interface';

export const getAllGroups = (req: Request, res: Response) => {
  const groups = groupServices.getAllGroups();
  res.status(200).json(groups);
};

export const getGroupById = (req: Request<{ id: string }>, res: Response) => {
  const { id } = req.params;
  const group = groupServices.getGroupById(id);
  res.status(200).json(group);
};

export const createGroup = (
  req: ValidatedRequest<IGroupCreateInterface>,
  res: Response,
) => {
  const group = groupServices.createGroup(req.body);
  res.status(201).json(group);
};

export const updateGroud = (
  req: ValidatedRequest<IGroupUpdateInterface>,
  res: Response,
) => {
  const { id } = req.params;
  const group = groupServices.updateGroupById(id, req.body);
  res.status(200).json(group);
};

export const deleteGroup = (
  req: Request<{id: string}>,
  res: Response
) => {
  const { id } = req.params;
  const group = groupServices.deleteGroupById(id);
  res.status(200).json(group);
}
