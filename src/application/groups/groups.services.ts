import * as groupModel from './groups.model';
import { IGroup } from './types/groups.interface';

export const getAllGroups = () => groupModel.getAllGroups();

export const getGroupById = (id: string) => groupModel.getGroupById(id);

export const createGroup = (createGroupSchema: Omit<IGroup, 'id'>) => {
  return groupModel.createGroup(createGroupSchema);
};

export const updateGroupById = (
  id: string,
  updateGroupSchema: Partial<IGroup>,
) => {
  return groupModel.updateGroupById(id, updateGroupSchema);
};

export const deleteGroupById = (id: string) => groupModel.deleteGroupById(id);
