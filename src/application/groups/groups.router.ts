import { Router } from 'express';
import * as groupController from './groups.controller';
import controllerWrapper from '../utils/controller-wrapper';
import validator from '../middlewares/validation.middleware';
import idParamSchema from '../schemas/id-param.schema';
import { groupCreateSchema, groupUpdateSchema } from './groups.schema';

const groupsRouter = Router();

groupsRouter.get('/', controllerWrapper(groupController.getAllGroups));

groupsRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupController.getGroupById),
);

groupsRouter.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupController.createGroup),
);

groupsRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupController.updateGroud),
);

groupsRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupController.deleteGroup),
);

export default groupsRouter;
