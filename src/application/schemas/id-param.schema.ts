import Joi from 'joi';

const idParamSchema = Joi.object<{ id: string }>({
  id: Joi.string().hex().length(24).required(),
});

export default idParamSchema;