import { NextFunction, Request, Response } from 'express';

const controllerWrapper = (requestHandller: any) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await requestHandller(req, res, next);
    } catch (error) {
      next(error);
    }
  };
};

export default controllerWrapper;
