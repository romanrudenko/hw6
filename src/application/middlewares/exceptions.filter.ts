import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/http-exceptions';
import { HttpStatuses } from '../enums/http-status.enums';

const exceptionFilter = (
  error: HttpException,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const status = error.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Something went wrong';
  res.status(status).send({ status, message });
};

export default exceptionFilter;
