import { HttpStatuses } from '../enums/http-status.enums';
import HttpException from '../exceptions/http-exceptions';
import { IStudent } from './types/students.interface';
import ObjectID from 'bson-objectid';
import { groups } from '../groups/groups.model';
import { group } from 'console';
import { IStudentResponse } from './types/student-response.interface';

export const students: IStudent[] = [
  {
    id: ObjectID().toHexString(),
    name: 'John',
    surname: 'Doe',
    email: 'john_doe@gmail.com',
    age: 21,
    groupId: '648c445c6ddab95788b877d7',
  },
  {
    id: ObjectID().toHexString(),
    name: 'Foo',
    surname: 'Bar',
    email: 'foo_bar@gmail.com',
    age: 20,
    groupId: '648c445c6ddab95788b877d7',
  },
];

export const getAllStudents = (): IStudentResponse[] => {
  const studentsWithGroupName = students.map((student) => {
    const group = groups.find((group) => student.groupId === group.id);
    const { groupId, ...studentsWithoutGroupId } = student;

    if (group) {
      return {
        ...studentsWithoutGroupId,
        groupName: group.name,
      };
    } else {
      return {
        ...studentsWithoutGroupId,
        groupName: null,
      };
    }
  });

  return studentsWithGroupName;
};

export const getStudentById = (
  studentId: string,
): IStudentResponse | undefined => {
  const student = students.find(({ id }) => id === studentId);
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const group = groups.find(({ id }) => id === student.groupId);
  const { groupId, ...studentsWithoutGroupId } = student;
  if (!group) {
    return {
      ...studentsWithoutGroupId,
      groupName: null,
    };
  }

  return {
    ...studentsWithoutGroupId,
    groupName: group.name,
  };
};

export const createStudent = (
  createStudentSchema: Omit<IStudent, 'id'>,
): IStudent => {
  const newStudent = {
    id: ObjectID().toHexString(),
    ...createStudentSchema,
  };

  if (newStudent.groupId === undefined) {
    newStudent.groupId = null;
  }

  students.push(newStudent);

  return newStudent;
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const updatedStudent = {
    ...student,
    ...updateStudentSchema,
  };

  students.splice(studentIndex, 1, updatedStudent);

  return updatedStudent;
};

export const addGroupToStudent = (
  studentId: string,
  groupId: string,
): IStudent | undefined => {
  const student = students.find(({ id }) => id === studentId);
  const group = groups.find(({id}) => id === groupId);

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  student.groupId = groupId;

  return student;
};

export const deleteStudentById = (studentId: string): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  students.splice(studentIndex, 1);

  return student;
};
