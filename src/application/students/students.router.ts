import { Router } from 'express';
import * as studentController from './students.controller';
import controllerWrapper from '../utils/controller-wrapper';
import validator from '../middlewares/validation.middleware';
import idParamSchema from '../schemas/id-param.schema';
import { studentCreateSchema, studentUpdateSchema } from './student.schema';
import uploadMiddleware from '../middlewares/upload.middleware';

const studentRouter = Router();

studentRouter.get('/', controllerWrapper(studentController.getAllStudents));

studentRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentController.getStudentById),
);

studentRouter.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentController.createStudent),
);

studentRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentController.updateStudentById),
);

studentRouter.patch(
  '/:id/group',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentController.addGroupToStudent),
);

studentRouter.patch(
  '/:id/image',
  validator.params(idParamSchema),
  uploadMiddleware.single('file'),
  controllerWrapper(studentController.addImage),
);

studentRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentController.deleteStudentById),
);

export default studentRouter;
