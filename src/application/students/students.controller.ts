import { Request, Response } from 'express';
import * as studentServices from './students.services';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentCreateInterface } from './types/student-create-request.interface';
import { IStudentUpdateInterface } from './types/student-update-request.interface';

export const getAllStudents = (req: Request, res: Response) => {
  const students = studentServices.getAllStudents();
  res.status(200).json(students);
};

export const getStudentById = (req: Request<{ id: string }>, res: Response) => {
  const { id } = req.params;
  const student = studentServices.getStudentById(id);
  res.status(200).json(student);
};

export const createStudent = (
  req: ValidatedRequest<IStudentCreateInterface>,
  res: Response,
) => {
  const student = studentServices.createStudent(req.body);
  res.status(201).json(student);
};

export const updateStudentById = (
  req: ValidatedRequest<IStudentUpdateInterface>,
  res: Response,
) => {
  const { id } = req.params;
  const student = studentServices.updateStudentById(id, req.body);
  res.status(200).json(student);
};

export const addImage = (
  req: Request<{ id: string; file: Express.Multer.File }>,
  res: Response,
) => {
  const { id } = req.params;
  const { path } = req.file ?? {};
  const student = studentServices.addImage(id, path);
  res.status(200).json(student);
};

export const addGroupToStudent = (
  req: Request<{ id: string; groupId: string }>,
  res: Response,
) => {
  const { id } = req.params;
  const { groupId } = req.body;
  const student = studentServices.addGroupToStudent(id, groupId);
  res.status(200).json(student);
};

export const deleteStudentById = (
  req: Request<{ id: string }>,
  res: Response,
) => {
  const { id } = req.params;
  const student = studentServices.deleteStudentById(id);
  res.status(200).json(student);
};
