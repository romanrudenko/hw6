import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../enums/http-status.enums';
import HttpException from '../exceptions/http-exceptions';
import * as studentModel from './students.model';
import { IStudent } from './types/students.interface';
import path from 'path';
import * as fs from 'node:fs/promises';

export const getAllStudents = () => studentModel.getAllStudents();

export const getStudentById = (id: string) => studentModel.getStudentById(id);

export const createStudent = (createStudentSchema: Omit<IStudent, 'id'>) => {
  return studentModel.createStudent(createStudentSchema);
};

export const updateStudentById = (
  id: string,
  updateStudentSchema: Partial<IStudent>,
) => {
  return studentModel.updateStudentById(id, updateStudentSchema);
};

export const addImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageId + imageExtention;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../../',
      'public',
      studentsDirectoryName,
    );

    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const addGroupToStudent = (studentId: string, groupId: string) => {
  return studentModel.addGroupToStudent(studentId, groupId);
};

export const deleteStudentById = (id: string) => {
  return studentModel.deleteStudentById(id);
};
