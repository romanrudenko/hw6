import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './students.interface';

export interface IStudentUpdateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}
