import { IStudent } from './students.interface';

export interface IStudentResponse {
  id: string;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath?: string;
  groupName: string | null;
}
