import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './students.interface';

export interface IStudentCreateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IStudent, 'id'>;
}
